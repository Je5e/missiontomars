open class Rocket : SpaceShip {
    open var cost = 0
    open val weight = 0
    open var maxWeight = 0
    open var currentWeight = 0

    override fun launch() = true

    override fun land() = true

    override fun canCarry(item: Item): Boolean {
        return this.currentWeight + item.weight <= this.maxWeight
    }

    override fun carry(item: Item) {
        this.currentWeight += item.weight
    }

}