import kotlin.random.Random

class U1 : Rocket() {
    override var cost = 100
    override val weight = 10000
    override var maxWeight = 18000
    override var currentWeight = weight

    override fun launch(): Boolean {
        val randomValue = (Random.nextDouble(from = 0.0, until = 1.0) * 100 + 1).toInt()
        val chanceOfExplosion = 5.0 * (this.currentWeight - this.weight) / (this.maxWeight - this.weight)
        return chanceOfExplosion <= randomValue
    }

    override fun land(): Boolean {
        val randomValue = (Random.nextDouble(from = 0.0, until = 1.0) * 100 + 1).toInt()
        val chanceOfCrash = 1.0 * (this.currentWeight - this.weight) / (this.maxWeight - this.weight)
        return chanceOfCrash <= randomValue
    }
}