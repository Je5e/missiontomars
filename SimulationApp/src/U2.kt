import kotlin.random.Random

class U2 : Rocket() {
    override var cost = 120
    override val weight = 18000
    override var maxWeight = 29000
    override var currentWeight = weight
    override fun launch(): Boolean {
        val randomValue = (Random.nextDouble(from = 0.0, until = 1.0) * 100 + 1).toInt()

        val chanceOfLaunchExplosion =
            4.0 * (this.currentWeight - this.weight) / (this.maxWeight - this.weight)

        return chanceOfLaunchExplosion <= randomValue
    }

    override fun land(): Boolean {
        val randomValue = (Random.nextDouble(from = 0.0, until = 1.0) * 100 + 1).toInt()
        val chanceOfLandingCrash =
            8.0 * (this.currentWeight - this.weight) / (this.maxWeight - this.weight)
        return chanceOfLandingCrash <= randomValue
    }
}