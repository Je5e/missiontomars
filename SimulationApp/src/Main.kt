import java.io.FileNotFoundException

fun main() {
    println("Welcome to the simulation of the Rockets...")
    val FILE_PATH_ITEMS_PHASE_1 = "phase-1.txt"
    val FILE_PATH_ITEMS_PHASE_2 = "phase-2.txt"
    var simulation = Simulation()

    try {
        var itemsForPhase1 = simulation.loadItems(FILE_PATH_ITEMS_PHASE_1)
        var itemsForPhase2 = simulation.loadItems(FILE_PATH_ITEMS_PHASE_2)
        var fleetsOfRocketU1ForPhase1 = simulation.loadU1(itemsForPhase1)
        println("Total of Rockets U1, phase 1: ${fleetsOfRocketU1ForPhase1.size}")
        var fleetsOfRocketU1ForPhase2 = simulation.loadU1(itemsForPhase2)
        println("Total of Rockets U1, phase 2: ${fleetsOfRocketU1ForPhase2.size}")
        for (r in fleetsOfRocketU1ForPhase2) {
            fleetsOfRocketU1ForPhase1.add(r)
        }
        println("Budget for Rockets U1, phase 1 and phase 2: ")
        var budgetU1Phase1 = simulation.runSimulation(fleetsOfRocketU1ForPhase1)
        println("$budgetU1Phase1 millions")
    } catch (e: FileNotFoundException) {
        println("Error: the file not found: ${e.message}")
    }

    try {
        var itemsForPhase1 = simulation.loadItems(FILE_PATH_ITEMS_PHASE_1)
        var itemsForPhase2 = simulation.loadItems(FILE_PATH_ITEMS_PHASE_2)
        var fleetsOfRocketU2ForPhase1 = simulation.loadU2(itemsForPhase1)
        println("Total of Rockets U2, phase 1: ${fleetsOfRocketU2ForPhase1.size}")
        var fleetsOfRocketU2ForPhase2 = simulation.loadU2(itemsForPhase2)
        println("Total of Rockets U2, phase 2: ${fleetsOfRocketU2ForPhase2.size}")
        for (r in fleetsOfRocketU2ForPhase2) {
            fleetsOfRocketU2ForPhase1.add(r)
        }
        println("Budget for Rockets U2, phase 1 and phase 2: ")
        var budgetU2Phase1 = simulation.runSimulation(fleetsOfRocketU2ForPhase1)
        println("$budgetU2Phase1 millions")
    } catch (e: FileNotFoundException) {
        println("Error the file not found: ${e.message}")
    }
}