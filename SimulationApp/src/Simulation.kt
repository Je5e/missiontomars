import java.io.File
import java.util.*

class Simulation {
    fun loadItems(path: String): ArrayList<Item> {
        val file = File(path)
        val scanner = Scanner(file)
        val items = ArrayList<Item>()

        while (scanner.hasNext()) {
            val line = scanner.nextLine()
            val values = line.split("=")
            items.add(Item(values[0], values[1].toInt()))
        }

        return items
    }

    fun loadU1(items: ArrayList<Item>): ArrayList<Rocket> {
        var U1rocket = U1()
        val fleetOfU1Rockets = ArrayList<Rocket>()
        fleetOfU1Rockets.add(U1rocket)

        for (item in items) {

            if (!U1rocket.canCarry(item)) {
                U1rocket = U1()
                fleetOfU1Rockets.add(U1rocket)

            }
            U1rocket.carry(item)
        }

        return fleetOfU1Rockets
    }

    fun loadU2(items: ArrayList<Item>): ArrayList<Rocket> {
        var U2rocket = U2()
        val fleetOfU2Rockets = ArrayList<Rocket>()
        fleetOfU2Rockets.add(U2rocket)

        for (item in items) {

            if (!U2rocket.canCarry(item)) {
                U2rocket = U2()
                fleetOfU2Rockets.add(U2rocket)

            }
            U2rocket.carry(item)
        }

        return fleetOfU2Rockets
    }

    fun runSimulation(rockets: ArrayList<Rocket>): Int {
        val requiredBudget: Int
        var countFailedLaunchOrLanding = 0
        for (rocket in rockets) {
            if (!rocket.launch()) {
                rocket.launch()
                countFailedLaunchOrLanding++
            }
            if (!rocket.land()) {
                rocket.land()
                countFailedLaunchOrLanding++
            }
        }
        requiredBudget =
            (rockets[0].cost * (rockets.size + countFailedLaunchOrLanding))
        return requiredBudget
    }

}